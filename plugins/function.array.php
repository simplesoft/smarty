<?php

function smarty_function_array($params, &$smarty)
{
    $array = $params['array'];
    if (!is_array($array)) {
        return 'param "array" is not an array';
    }

    $function = $params['function'];
    switch ($function) {
        case 'current':
            return current($array);
    }
    return false;
}

/* vim: set expandtab: */

?>
